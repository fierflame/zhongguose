function getPath(v) {
  if (!v) {
    return "";
  }
  if (v >= 100) {
    return "M 12 4.5 A 7.5 7.5 0 0 0 12 19.5 A 7.5 7.5 0 0 0 12 4.5";
  }
  const x = 12 + 7.5 * Math.sin(v / 50 * Math.PI);
  const y = 12 - 7.5 * Math.cos(v / 50 * Math.PI);
  return `M 12 4.5 A 7.5 7.5 0 ${v > 50 ? 1 : 0} 1 ${x} ${y}`;
}
export default function createArc(v, color) {
  const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.setAttribute("width", "24");
  svg.setAttribute("height", "24");
  const bg = svg.appendChild(document.createElementNS("http://www.w3.org/2000/svg", "path"));
  bg.setAttribute("d", "M 12 4.5 A 7.5 7.5 0 0 0 12 19.5 A 7.5 7.5 0 0 0 12 4.5");
  bg.setAttribute("fill", "none");
  bg.setAttribute("stroke", "#FFF3");
  bg.setAttribute("stroke-width", "9");
  if (!v) {
    return svg;
  }
  const cl = document.createElementNS("http://www.w3.org/2000/svg", "path");
  svg.appendChild(cl);
  cl.setAttribute("d", getPath(v));
  cl.setAttribute("fill", "none");
  cl.setAttribute("stroke", color || "currentcolor");
  cl.setAttribute("stroke-width", "9");
  return svg;
}
