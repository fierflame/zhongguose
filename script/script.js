import colors from "./colors.js";
import nav, {oninput} from "./nav.js";
import main, {renderColor} from "./main.js";
import h from "./createElement.js";
function find(pinyin) {
  return colors.find((color) => color.pinyin === pinyin);
}
function hashRenderColor() {
  var colorPinyin = location.hash.substring(1);
  if (!colorPinyin) {
    return;
  }
  renderColor(find(colorPinyin));
}
;
hashRenderColor();
addEventListener("hashchange", hashRenderColor);
document.body.appendChild(main);
document.body.appendChild(/* @__PURE__ */ h("input", {
  oninput,
  id: "filter",
  placeholder: "过滤颜色"
}));
document.body.appendChild(nav);
document.body.appendChild(/* @__PURE__ */ h("footer", null));
