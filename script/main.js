import h from "./createElement.js";
const nameEl = /* @__PURE__ */ h("span", {
  id: "name"
}, "中国色");
const pinyinEl = /* @__PURE__ */ h("span", {
  id: "pinyin"
}, "ZHONGGUOSE");
const CMYKcolor = /* @__PURE__ */ h("div", {
  id: "CMYKcolor"
}, /* @__PURE__ */ h("span", null, "0"), /* @__PURE__ */ h("span", null, "0"), /* @__PURE__ */ h("span", null, "0"), /* @__PURE__ */ h("span", null, "0"));
const RGBcolor = /* @__PURE__ */ h("div", {
  id: "RGBcolor"
}, "#", /* @__PURE__ */ h("span", {
  "data-dec": "255"
}, "FF"), /* @__PURE__ */ h("span", {
  "data-dec": "255"
}, "FF"), /* @__PURE__ */ h("span", {
  "data-dec": "255"
}, "FF"));
export function renderColor(color) {
  if (!color) {
    return;
  }
  const {hex, RGB, CMYK, name, pinyin} = color;
  document.body.style.backgroundColor = hex;
  pinyinEl.innerHTML = pinyin.toUpperCase();
  nameEl.innerHTML = name;
  const [r, g, b] = RGBcolor.querySelectorAll("span");
  r.innerHTML = RGB[0].toString(16).padStart(2, "0").toUpperCase();
  g.innerHTML = RGB[1].toString(16).padStart(2, "0").toUpperCase();
  b.innerHTML = RGB[2].toString(16).padStart(2, "0").toUpperCase();
  r.dataset.dec = String(RGB[0]);
  g.dataset.dec = String(RGB[1]);
  b.dataset.dec = String(RGB[2]);
  const [c, m, y, k] = CMYKcolor.children;
  c.innerHTML = String(CMYK[0]);
  m.innerHTML = String(CMYK[1]);
  y.innerHTML = String(CMYK[2]);
  k.innerHTML = String(CMYK[3]);
}
export default /* @__PURE__ */ h("main", {
  id: "data"
}, pinyinEl, nameEl, RGBcolor, CMYKcolor);
