window.onerror = (...p) => {
  const div = document.body.appendChild(document.createElement("div"));
  div.innerText = p.join("\n");
  div.style.writingMode = "horizontal-tb";
  div.style.whiteSpace = "pre-line";
};
