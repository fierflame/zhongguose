import rgb2hsv from "./rgb2hsv.js";
const response = await fetch("./colors.json");
const data = await response.json();
export default data.sort((a, b) => {
  const [ha, sa, va] = rgb2hsv(a.RGB);
  const [hb, sb, vb] = rgb2hsv(b.RGB);
  return ha === hb ? sb - sa : hb - ha;
});
