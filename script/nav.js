import h from "./createElement.js";
import createArc from "./createArc.js";
import colors from "./colors.js";
function create({hex, pinyin, name, CMYK, RGB}) {
  return /* @__PURE__ */ h("a", {
    style: {"--color": hex},
    href: "#" + pinyin.toLowerCase()
  }, /* @__PURE__ */ h("span", {
    class: "name"
  }, name), /* @__PURE__ */ h("span", {
    class: "cmyk"
  }, CMYK.map((v) => createArc(v))), /* @__PURE__ */ h("span", {
    class: "pinyin"
  }, pinyin), /* @__PURE__ */ h("span", {
    class: "rgb-lines"
  }, RGB.map((v) => /* @__PURE__ */ h("span", {
    class: "rgb-line",
    style: {"--percents": `${v / 255 * 100}%`}
  }))), /* @__PURE__ */ h("span", {
    class: "rgb"
  }, hex.replace("#", "")));
}
const nav = /* @__PURE__ */ h("nav", {
  id: "colors"
});
let visible = colors;
export default nav;
export function oninput(e) {
  const value = e?.target?.value || "";
  if (!value) {
    visible = colors;
    nav.style.blockSize = `${visible.length * 60}px`;
    return;
  }
  const regex = new RegExp(value.split("").map((v) => "^&*+?(){}[]|\\.".includes(v) ? `\\${v}` : v).join(".*"), "i");
  visible = colors.filter((v) => regex.test(`${v.name}${v.pinyin}${v.hex}`));
  nav.style.blockSize = `${visible.length * 60}px`;
}
const map = new Map();
function get(v) {
  const e = map.get(v);
  if (e) {
    return e;
  }
  const el = create(v);
  map.set(v, el);
  return el;
}
let showed = new Set();
function updateList() {
  const {innerHeight, innerWidth} = window;
  const def = innerWidth > innerHeight;
  const scroll = def ? Math.abs(window.scrollY) - 40 : Math.abs(window.scrollX);
  const min = Math.floor(scroll / 60) - 10;
  const max = Math.floor((scroll + (def ? innerHeight : innerWidth)) / 60) + 10;
  const needRemove = new Set(showed);
  showed = new Set();
  for (let k = Math.max(0, min); k <= max && k < visible.length; k++) {
    const el = get(visible[k]);
    el.style.insetBlockStart = `${k * 60 + 5}px`;
    showed.add(el);
    if (!needRemove.delete(el))
      nav.appendChild(el);
  }
  for (const e of needRemove) {
    e.remove();
  }
}
function update() {
  requestAnimationFrame(update);
  updateList();
}
oninput();
update();
