export default function createElement(tagName, options, ...childNodes) {
  const el = document.createElement(tagName);
  for (const [key, value] of Object.entries(options || {})) {
    if (typeof value === "function") {
      if (key.substring(0, 2) === "on") {
        el.addEventListener(key.substring(2), value);
      }
      continue;
    }
    if (key === "style") {
      if (!value) {
        continue;
      }
      if (typeof value === "string") {
        el.setAttribute(key, value);
        continue;
      }
      if (typeof value === "object") {
        for (const [k, v] of Object.entries(value)) {
          if (!v) {
            continue;
          }
          el.style.setProperty(k, String(v));
        }
      }
      continue;
    }
    if (value === true) {
      el.setAttribute(key, "");
      continue;
    }
    if (["string", "bigint", "number"].includes(typeof value)) {
      el.setAttribute(key, String(value));
      continue;
    }
    if (typeof value !== "object") {
      continue;
    }
    if (key !== "dataset" && key === "data") {
      continue;
    }
    for (const [k, v] of Object.entries(value)) {
      if (v === true) {
        el.dataset[k] = "";
        continue;
      }
      if (["string", "bigint", "number"].includes(typeof v)) {
        el.dataset[k] = String(v);
        continue;
      }
    }
  }
  for (const child of childNodes.flat(Infinity)) {
    if (child instanceof Node) {
      el.appendChild(child);
      continue;
    }
    if (["string", "bigint", "number"].includes(typeof child)) {
      el.appendChild(document.createTextNode(String(child)));
    }
  }
  return el;
}
